$fn=1000;

////////////////// TARGETS //////////////////
//projection()
//rotate([0,-90,0])
//rotate([180,0,0])
//trappe();
//bolt();
negative();

//////////////////////////////////////////////
/* Log
v 05 a:
* hole and tab are shifted wrong way,
* curvature is too high, radius needs to be increased
* plate is too thick  by 0.4 - 0.5 mm 
v 06
* reduced plate thickness
* increased plate curve radius
* inverted shifts
* rotated tab to be tangental to plate
*/

radiusCorrectionFactor =  1.4;
thicknessReduction     = 0.45;
rExt      = 150*radiusCorrectionFactor;
thickness = 3-thicknessReduction;
maxH      = 150;

projR        = 104.5/2.;//52.5;  //50;
deltaCenters = 23; //30;

holeOffset      = 10;
holeDia         = 8.5;
holeRecessDia   = 15;
holeRecessDepth = 2;
holeShiftY      = 2.5;   //-2.5;  // chzanged sign in v 06

tabStandoffZ = 1;
tabLength   = 15;
tabWidth    = 10;
tabOverhang = 5;
tabThickness = 2;
tabAlpha     = -1;
tabEpsilon   = 0.77;//1.;//0.06; //0.09;
tabShiftY    = -2; //2;  // changed sign in v 06
tabShiftAlpha = -1.2;
tabXAlpha     = -2;

module tabRaw(){
  x = (tabLength-tabThickness)/2.;
  y = (tabWidth-tabThickness)/2.;
  p1 = [x,y,0];
  p2 = [-x,y,0];
  p3 = [x,-y,0];
  p4 = [-x,-y,0];
  rotate([0,0,tabShiftAlpha])
  translate([-tabThickness/2.,0,0])
  rotate([0,90+tabAlpha,0])
    translate([-(tabLength/2.-tabOverhang),0,0])
      union(){
        hull()
          for (p=[p1,p2,p3,p4])
            translate(p)
              sphere(d=tabThickness);
        tabStandoff();
      }
}
//tabRaw();

module tabStandoff(){
  x = (tabLength-tabThickness)/2.;
  y = (tabWidth-tabThickness)/2.;
  p1 = [0,y,0];
  p2 = [-x,y,0];
  p3 = [0,-y,0];
  p4 = [-x,-y,0];
  hull()
    for (p=[p1,p2,p3,p4])
      translate(p)
        translate([0,0,tabStandoffZ])
          cylinder(h=tabThickness,d=tabThickness,center =true);
}
//tabStandoff();

module tab(epsilon=tabEpsilon){
  translate([0,tabShiftY,0])
    translate([rExt-thickness-epsilon,0,-projR-deltaCenters/2.-1.3])
     rotate([tabXAlpha,0,0])
      tabRaw();
}
//tab();

module cylExt(){
  cylinder(h=maxH,r =rExt,center=true);
}
//cylExt();

module cylInt(){
  cylinder(h=maxH,r =rExt-thickness,center=true);
}
//cylInt();

module rawShell(){
  difference(){
    cylExt();
    cylInt();
  }
}
//rawShell();

module cutter(){
  rotate([0,90,0])
    linear_extrude(600)
      hull(){
        translate([-deltaCenters/2.,0,0])
          circle(projR);
        translate([deltaCenters/2.,0,0])
          circle(projR);
      }
}
//cutter();

module boltHoleCutter(){
  rotate([0,-90,0])
    linear_extrude(thickness)
      circle(d=holeDia);
}
//boltHoleCutter();

module recessCutter(){
  rotate([0,90,0])
    linear_extrude(holeRecessDepth)
      circle(d=holeRecessDia);
}
//recessCutter();

module holeCutter(){
translate([0,holeShiftY,0])
  translate([rExt-holeRecessDepth,0,0])
  translate([0,0,deltaCenters/2.+projR-holeOffset])
    union(){
      boltHoleCutter();
      recessCutter();
    }
}
//holeCutter();

module trappe(doTab=true){
  if (doTab)
    tab();
  difference(){
    intersection(){
      rawShell();
      cutter();
      }
    holeCutter();
  }
}
//trappe();

module positionTrappe(doTab){
  rotate([0,90,0])
    translate([-210,0,0])
      trappe(doTab);
}
//positionTrappe();

ZZ=5;
module sliceProjectionUpper(doTab=false,thickness = ZZ){
  translate([0,0,10])
    linear_extrude(thickness)
      projection()
        positionTrappe(doTab);
}
//hull(){
//sliceProjectionUpper();

module tabber(){
  difference(){
    positionTrappe(true);
    positionTrappe(false);
  }
  z=4.55;
  translate([0,0,z])
    linear_extrude(ZZ+1+z)
      projection()
        difference(){
          positionTrappe(true);
          positionTrappe(false);
        }
}
//tabber();
       

module upperHull(){
  hull(){
  sliceProjectionUpper(thickness=ZZ);
  positionTrappe();
  }
}

module holeCyl1(){
  difference(){
    translate([53,2.5,0])
      cylinder(h=2.55,d=20);
    //sliceProjectionUpper();
    positionTrappe();
  }
}
//holeCyl1();

module holeCylUpper(){
  linear_extrude(ZZ*2)
    projection(cut=true)
      translate([0,0,-2])
        holeCyl1();
}
//holeCylUpper();

module fullUpper(){
  difference(){
    upperHull();
    //holeCyl1();
    //holeCylUpper();
  }
  tabber();
}
//fullUpper();

module outerBlock(){
  x=200;
  y=120;
  z=ZZ*3;
  r = 5;
  translate([-x/2,-y/2,-ZZ])
    //cube([x,y,z]);
    roundedBlock(x,y,z,r);
}
//outerBlock();

module roundedBlock(x,y,z,r){
  points = [[r,r,0],
            [r,y-r,0],
            [x-r,r,0],
            [x-r,y-r,0]];
  hull()
    for (p=points)
      translate(p)
        cylinder(h=z,r=r);
}

module negative(){
  difference(){
    outerBlock();
    fullUpper();
  }
}
//negative();
  
///////////////////////  BOLT ////////////////

boltDFXFileName = "DXF/bolt c.dxf";
boltProfileLayerName = "profile";

boltSlotX = 2;
boltSlotY = 15;
boltSlotZ = 1.0;

boltCylD = 2;
boltSpiralBaseZ = 9;
boltSpiralZ = 5.8;

module boltRaw(){
  rotate_extrude()
    import(boltDFXFileName,layer=boltProfileLayerName);
}
//boltRaw();

module boltSlot(){
  translate([0,0,boltSlotZ/2.*0.99])
  cube([boltSlotX,boltSlotY,boltSlotZ],center=true);
}
//boltSlot();

module bolt(){
  difference(){
    boltRaw();
    boltSlot();
    spiral();
    lowCyl();
    //lowCyl(-14,4);
  }
}
//bolt();

module spiral(){
  translate([0,0,boltSpiralBaseZ])
    linear_extrude(height = boltSpiralZ,  convexity=10,twist=-90)
      projection()
        rotate([0,90,0])
          cylinder(h=10,d=boltCylD,center=true);
}
//spiral();

module lowCyl(rot=-0,fact=8){
  translate([0,0,boltSpiralBaseZ+boltCylD/fact])
    rotate([0,90,rot])
      cylinder(h=10,d=boltCylD*1.22,center=true);
}
